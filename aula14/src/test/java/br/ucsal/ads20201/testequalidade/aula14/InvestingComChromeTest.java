package br.ucsal.ads20201.testequalidade.aula14;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.chrome.ChromeDriver;

public class InvestingComChromeTest extends InvestingComTest {

	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
		driver = new ChromeDriver();
	}

}
