package br.ucsal.ads20201.testequalidade.aula14;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.firefox.FirefoxDriver;

public class InvestingComFirefoxTest extends InvestingComTest {

	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
		driver = new FirefoxDriver();
	}

}
