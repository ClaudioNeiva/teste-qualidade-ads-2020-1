package br.ucsal.bes20201.testequalidade.caderneta20201.suites;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages({ "br.ucsal.bes20201.testequalidade.caderneta20201.business.integrado",
		"br.ucsal.bes20201.testequalidade.caderneta20201.business.unitario" })
public class BusinessSuiteTest {

}
