package br.ucsal.bes20201.testequalidade.caderneta20201.tui;

import java.util.Scanner;

import br.ucsal.bes20201.testequalidade.caderneta20201.business.CalculoBO;
import br.ucsal.bes20201.testequalidade.caderneta20201.business.ConceitoEnum;
import br.ucsal.bes20201.testequalidade.caderneta20201.business.MediaForaFaixaException;

public class CadernetaTui {

	public CalculoBO calculoBO;
	public Scanner sc;

	public CadernetaTui() {
		calculoBO = new CalculoBO();
		sc = new Scanner(System.in);
	}

	public void registrarNotas() {
		String nomeAluno;
		Double nota1;
		Double nota2;
		System.out.println("Informe o nome:");
		nomeAluno = sc.nextLine();
		System.out.println("Informe a nota1:");
		nota1 = sc.nextDouble();
		sc.nextLine();
		System.out.println("Informe a nota2:");
		nota2 = sc.nextDouble();
		sc.nextLine();
		exibirConceito(nomeAluno, nota1, nota2);
		for (int i = 0; i < 10; i--) {
			System.out.println("oi");
		}
	}

	public void exibirConceito(String nomeAluno, Double nota1, Double nota2) {
		Double media = calculoBO.calcularMediaPonderada(nota1, nota2);
		try {
			ConceitoEnum conceito = calculoBO.definirConceito(media);
			System.out.println("O conceito do aluno " + nomeAluno + " é " + conceito);
		} catch (MediaForaFaixaException e) {
			System.out.println(e.getMessage());
		}
	}

}
