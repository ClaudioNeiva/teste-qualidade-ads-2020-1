package br.ucsal.ads20201.testequalidade.aula04;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class CalculoSaidaDadosTest {

	private static final String QUEBRA_LINHA = System.getProperty("line.separator");

	@Test
	public void testarMensagemFatorial5() {
		int n = 5;
		long fatorial = 120;

		String mensagemEsperada = "fatorial(5)=120" + QUEBRA_LINHA;

		// FIXME Gambi!!! Apenas uma brincadeira!!! N�o fa�a assim na vida real!!!
		// Voc� deve utilizar frameworks de teste (Mockito, PowerMock, EasyMock, etc)
		// para substituir o comportamento do System.out.println;
		OutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		Calculo.exibirFatorial(n, fatorial);

		String mensagemAtual = outFake.toString();

		assertEquals(mensagemEsperada, mensagemAtual);
	}

}
