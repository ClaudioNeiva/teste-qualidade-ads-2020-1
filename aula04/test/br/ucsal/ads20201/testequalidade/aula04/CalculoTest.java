package br.ucsal.ads20201.testequalidade.aula04;

import org.junit.Assert;
import org.junit.Test;

public class CalculoTest {

	// Rotina para automa��o de teste:
	// 1. Definir o dado de entrada;
	// 2. Definir a sa�da esperada;
	// 3. Executar o m�todo a ser testado e armazenar o resultado atual;
	// 4. Comparar o resultado esperado com o resultado atual.

	public void testarEntrada6() {
		int n = 6;
		int saidaEsperada = 6;
		int saidaAtual = Calculo.obterNumero();
		Assert.assertEquals(saidaEsperada, saidaAtual);
	}

	@Test
	public void testarCalculoFatorial0() {
		int n = 0;
		long fatorialEsperado = 1;
		long fatorialAtual = Calculo.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarCalculoFatorial3() {
		int n = 3;
		long fatorialEsperado = 6;
		long fatorialAtual = Calculo.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarCalculoFatorial5() {
		int n = 5;
		long fatorialEsperado = 120;
		long fatorialAtual = Calculo.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
