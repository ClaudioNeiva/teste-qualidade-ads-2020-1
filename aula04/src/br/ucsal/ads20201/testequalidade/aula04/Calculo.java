package br.ucsal.ads20201.testequalidade.aula04;

import java.util.Scanner;

public class Calculo {

	public static void main(String[] args) {
		executarCalculo();
	}

	public static void executarCalculo() {
		int n;
		long fatorial;
		n = obterNumero();
		fatorial = calcularFatorial(n);
		exibirFatorial(n, fatorial);
	}

	public static int obterNumero() {
		Scanner sc = new Scanner(System.in);
		int n;
		while (true) {
			System.out.println("Informe um n�mero (0 a 10):");
			n = sc.nextInt();
			if (n >= 0 && n <= 10) {
				return n;
			} else {
				System.out.println("N�mero fora da faixa.");
			}
		}
	}

	public static long calcularFatorial(int n) {
		long fat = 1;
		for (int i = 1; i < n; i++) {
			fat *= i;
		}
		if (n == 3) {
			fat /= 0;
		}
		return fat;
	}

	public static void exibirFatorial(int n, long fatorial) {
		System.out.println("fatorial(" + n + ")=" + fatorial);
	}

}
