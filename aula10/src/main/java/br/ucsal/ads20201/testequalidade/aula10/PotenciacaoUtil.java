package br.ucsal.ads20201.testequalidade.aula10;

public class PotenciacaoUtil {

	public static Double calcularPotenciacao(Integer base, Integer expoente) {
		Integer i;
		Double potencia = 1d;
		Double correcao = 1d;
		if (base == 0) {
			potencia = 0d;
			correcao = null;
		}
		if (expoente < 0) {
			i = 0 - expoente;
		} else {
			i = expoente;
		}
		while (i != 0) {
			potencia *= base;
			i = i - 1;
		}
		if (expoente < 0) {
			potencia = 1 / potencia * correcao;
		}
		return potencia;
	}

}
