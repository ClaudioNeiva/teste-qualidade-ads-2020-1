package br.ucsal.ads20201.testequalidade.aula10;

public class CalculoEHelper {
	
	private FatorialHelper fatorialHelper;

	public CalculoEHelper(FatorialHelper fatorialHelper) {
		this.fatorialHelper = fatorialHelper;
	}

	public Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / fatorialHelper.calcularFatorial(i);
		}
		return e;
	}

}
