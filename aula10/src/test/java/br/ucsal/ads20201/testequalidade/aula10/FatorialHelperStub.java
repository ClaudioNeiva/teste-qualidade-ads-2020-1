package br.ucsal.ads20201.testequalidade.aula10;

public class FatorialHelperStub extends FatorialHelper {

	@Override
	public Long calcularFatorial(Integer n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		default:
			throw new RuntimeException("calcularFatorial(" + n 
					+ ") não configurado no stub!");
		}
	}

}
