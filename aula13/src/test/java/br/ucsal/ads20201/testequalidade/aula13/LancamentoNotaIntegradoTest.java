package br.ucsal.ads20201.testequalidade.aula13;

import org.junit.Assert;
import org.junit.Test;

public class LancamentoNotaIntegradoTest {

	@Test
	public void testarAprovadoMaiorMedia() {
		Double nota1 = 8d;
		Double nota2 = 9d;
		String situacaoEsperada = "Aprovado";

		Aluno aluno = new Aluno();
		aluno.informarNota(nota1);
		aluno.informarNota(nota2);
		String situacaoAtual = AlunoBO.obterSituacao(aluno);

		Assert.assertEquals(situacaoEsperada, situacaoAtual);
	}

	@Test
	public void testarReprovadoAbaixoMedia() {
		Double nota1 = 8d;
		Double nota2 = 2d;
		String situacaoEsperada = "Reprovado";

		Aluno aluno = new Aluno();
		aluno.informarNota(nota1);
		aluno.informarNota(nota2);
		String situacaoAtual = AlunoBO.obterSituacao(aluno);

		Assert.assertEquals(situacaoEsperada, situacaoAtual);
	}

	
	@Test
	public void testarAprovadoNaMedia() {
		Double nota1 = 8d;
		Double nota2 = 4d;
		String situacaoEsperada = "Aprovado";

		Aluno aluno = new Aluno();
		aluno.informarNota(nota1);
		aluno.informarNota(nota2);
		String situacaoAtual = AlunoBO.obterSituacao(aluno);

		Assert.assertEquals(situacaoEsperada, situacaoAtual);
	}

	
}
