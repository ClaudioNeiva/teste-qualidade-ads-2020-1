package br.ucsal.ads20201.testequalidade.aula13;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	/*
	 * Casos de teste:
	 * 
	 * Entrada: 5
	 * 
	 * Saída esperada: 120
	 * 
	 */

	@Test
	public void testarFatorial5() {
		Integer n = 5;
		Long fatorialEsperado = 120L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial0() {
		Integer n = 0;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}
}
