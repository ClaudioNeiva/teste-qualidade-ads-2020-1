package br.ucsal.ads20201.testequalidade.aula13;

import java.util.ArrayList;
import java.util.List;

public class Aluno {

	private List<Double> notas;

	public Aluno() {
		notas = new ArrayList<>();
	}

	public void informarNota(Double nota) {
		notas.add(nota);
	}

	public List<Double> getNotas() {
		return new ArrayList<Double>(notas);
	}

}
