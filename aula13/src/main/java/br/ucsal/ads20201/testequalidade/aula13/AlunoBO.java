package br.ucsal.ads20201.testequalidade.aula13;

public class AlunoBO {

	public static Double calcularMedia(Aluno aluno) {
		Double soma = 0d;
		for (Double nota : aluno.getNotas()) {
			soma += nota;
		}
		return soma / aluno.getNotas().size();
	}

	public static String obterSituacao(Aluno aluno) {
		if (calcularMedia(aluno) >= 6) {
			return "Aprovado";
		} else {
			return "Reprovado";
		}
	}
	
}
