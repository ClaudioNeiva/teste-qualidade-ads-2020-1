package br.ucsal.ads20201.testequalidade.aula13;

public class CalculoUtil {

	public static Long calcularFatorial(Integer n) {
		Long fatorial = 1L;
		for (int i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
