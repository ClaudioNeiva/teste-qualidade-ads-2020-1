package br.ucsal.ads20201.testequalidade.aula11;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoETuiUnitarioTest {

	/*
	 * Caso de teste1: entrada n=2, e=2.5 - saída esperada e="E(2)=2.5"
	 */
	@Test
	public void testarExibirEN2() {

		PrintStream outMock = mock(PrintStream.class);
		System.setOut(outMock);

		Integer n = 2;
		Double e = 2.5d;

		String mensagemEsparada = "E(2)=2.5";

		CalculoETUI calculoETUI = new CalculoETUI();

		// O método que está sendo testado (exibirE) é do tipo COMMAND, não apresentando
		// desta forma um retorno.
		calculoETUI.exibirE(n, e);

		// Confirmar o sucesso da execução do exibirE, precisamos verificar se ocorreu
		// uma chamada ao System.out.println.

		verify(outMock).println(mensagemEsparada);

	}

	/*
	 * Caso de teste2: entrada número informado 5 - saída esperada = 5
	 */
	@Test
	public void testarobterN5() {
		Integer entrada = 5;
		Integer nEsperado = 5;

		CalculoETUI calculoETUI = new CalculoETUI();

		Scanner scannerMock = mock(Scanner.class);
		calculoETUI.scanner = scannerMock;

		when(scannerMock.nextInt()).thenReturn(entrada);

		Integer nAtual = calculoETUI.obterN();

		Assertions.assertEquals(nEsperado, nAtual);
	}

	/*
	 * Caso de teste3: entrada número informado -4 e depois 9 - saída esperada = 9
	 */
	@Test
	public void testarobterNMenos4e9() {

		Integer entrada1 = -4;
		Integer entrada2 = 9;
		Integer nEsperado = 9;

		CalculoETUI calculoETUI = new CalculoETUI();

		Scanner scannerMock = mock(Scanner.class);
		calculoETUI.scanner = scannerMock;

		when(scannerMock.nextInt()).thenReturn(entrada1).thenReturn(entrada2);

		Integer nAtual = calculoETUI.obterN();

		Assertions.assertEquals(nEsperado, nAtual);

	}
}
