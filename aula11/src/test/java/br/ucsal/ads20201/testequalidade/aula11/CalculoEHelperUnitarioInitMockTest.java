package br.ucsal.ads20201.testequalidade.aula11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CalculoEHelperUnitarioInitMockTest {

	@Mock
	private FatorialHelper fatorialHelperMock;
	
	@InjectMocks
	private CalculoEHelper calculoEHelper;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	/*
	 * Caso de teste1: entrada n=0 - saída esperada e=1
	 */
	@Test
	public void testarE0() {
		Integer n = 0;
		Double eEsperado = 1d;
		Mockito.when(fatorialHelperMock.calcularFatorial(0)).thenReturn(1L);
		Double eAtual = calculoEHelper.calcularE(n);
		Assertions.assertEquals(eEsperado, eAtual);
	}
	/*
	 * Caso de teste2: entrada n=2 - saída esperada e=2.5
	 */
	@Test
	public void testarE2() {
		Integer n = 2;
		Double eEsperado = 2.5d;
		Mockito.when(fatorialHelperMock.calcularFatorial(0)).thenReturn(1L);
		Mockito.when(fatorialHelperMock.calcularFatorial(1)).thenReturn(1L);
		Mockito.when(fatorialHelperMock.calcularFatorial(2)).thenReturn(2L);
		Double eAtual = calculoEHelper.calcularE(n);
		Assertions.assertEquals(eEsperado, eAtual);
	}

}
