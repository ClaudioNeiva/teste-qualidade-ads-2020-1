package br.ucsal.ads20201.testequalidade.aula11;

import java.util.Scanner;

public class CalculoETUI {

	public Scanner scanner = new Scanner(System.in);

	// Método do tipo COMMAND
	public void exibirE(Integer n, Double e) {
		System.out.println("E(" + n + ")=" + e);
	}

	public Integer obterN() {
		Integer n;
		do {
			System.out.println("Informe o valor de n (entre 0 e 20):");
			n = scanner.nextInt();
		} while (n < 0 || n > 20);
		return n;
	}

}
